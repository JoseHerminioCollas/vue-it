const menuLinkData = {
  links: [
    {
      text: 'Reporting',
      children: [
        { text: '121' },
        { text: 'ASAP' },
        { text: 'G ASAP I' },
        { text: 'F A I R' },
      ]
    },
    {
      text: 'A C',
      children: [
        { text: 'abc' },
        { text: 'ww' },
        { text: 'xxaa' },
      ]
    },
    {
      text: 'K C',
      children: [
      ]
    },
    {
      text: 'P S D S',
      children: [
      ]
    },
    {
      text: 'JKL',
      children: [
        { text: 'abc' },
        { text: '111' },
        { text: 'xx' },
      ]
    },
  ]
}

export default menuLinkData
