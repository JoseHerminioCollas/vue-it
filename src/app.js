import LinkMenu from './components/LinkMenu/index.vue'

export default {
  name: 'app',
  components: {
    LinkMenu,
  },
  methods: {
    toggleMainMenu: function toggleMainMenu() {
      if (this.hideMainMenu === false) {
        document.querySelector('.menu-bar').style.display = 'none'
        this.hideMainMenu = true
      }
      else {
        document.querySelector('.menu-bar').style.display = 'flex'
        this.hideMainMenu = false
      }

    },
    hideMenuItem: function hideMenuItem() {
      Array.from(document.querySelectorAll('.menu-item'))
        .map(e => {
          e.classList.remove('view-pos') 
          return e
        })
    },
    methodA: function methodA(event) { // show menu
      const elId = `#${event.target.value}`
      document.querySelector(elId).classList.add('view-pos')
    }
  },
  data: () => {
    return {
      hideMainMenu: true,
    }
  },
  props: {
    ab: String,
    menuData: Object
  },
}
