import Vue from 'vue'
import App from './App.vue'
import menuData from './menu-data'

new Vue({
  render: h => {
    return h(App, {
      props: {
        ab: 'xxx',
        menuData
      }
    })
  },
}).$mount('#app')
