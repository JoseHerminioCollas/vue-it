const menuData = {
  linkMenus: {
    report: {
      text: 'Reporting',
      children: [
        { text: '121' },
        { text: 'ASAP' },
        { text: 'G ASAP I' },
        { text: 'F A I R' },
      ]
    },
    ac: {
      text: 'A C',
      children: [
      ]
    },
    kc: {
      text: 'K C',
      children: [
      ]
    },
    psds: {
      text: 'P S D S',
      children: [
      ]
    },
    jkl: {
      text: 'JKL',
      children: [
        { text: 'abc' },
        { text: '111' },
        { text: 'xx' },
      ]
    },
    oji: { // admin menu
      text: 'OJI',
      children: [
        { text: 'Lorum' },
      ]
    },
    pay: {
      text: 'PAY',
      children: [
        { text: 'Ipsum' },
        { text: 'Ipsum 2' },
      ]
    },
    per: {
      text: 'PER',
      children: [
        { text: 'Delor' },
      ]
    },
    irdir: {
      text: 'IRDIR',
      children: [
        { text: 'Itsap' },
      ]
    },
    mandw: {
      text: 'MANDW',
      children: [
        { text: '1 18' },
      ]
    },
    afa: {
      text: 'AFA',
      children: [
      ]
    },
  }
}

export default menuData
