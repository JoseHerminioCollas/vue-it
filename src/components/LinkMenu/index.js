export default {
  name: 'LinkMenu',
  props: {data: Object},
  methods: {
    toggleMenu: function () {
      if (!this.hideChildren) {
        this.hideChildren = true
      } else {
        this.hideChildren = false
      }
    }
  },
  data: () => {
    return {
      hideChildren: true,
    }
  },
}
